use movie_db;


# 4. Show the films that Orlando Bloom and Ian McKellen have act together and has more than 2 Oscars.
select title
from movies
where oscars>2 and movie_id in(
select movie_id
from movie_stars join stars on movie_stars.star_id=stars.star_id
where star_name="Orlando Bloom" and movie_id in(
	select movie_id
	from movie_stars join stars on movie_stars.star_id=stars.star_id
	where star_name="Ian McKellen"));
	
select movie_stars.movie_id
from movie_stars join stars on movie_stars.star_id=stars.star_id
	join(select movie_id
	from movie_stars join stars on movie_stars.star_id=stars.star_id
	where star_name="Ian McKellen") as t2 on movie_stars.movie_id=t2.movie_id
where star_name="Orlando Bloom";

# 10.Compute the average budget of the films directed by Peter Jackson.
select avg(budget)
from movies
where movie_id in (
	select movie_id
	from movie_directors join directors on movie_directors.director_id=directors.director_id
	where director_name="Peter Jackson");
    
    
# 11.Show the Francis Ford Coppola film that has the minimum budget.
select title,budget
from movies
where movie_id in (
	select movie_id
	from movie_directors join directors on movie_directors.director_id=directors.director_id
	where director_name="Francis Ford Coppola")
order by budget
limit 1;